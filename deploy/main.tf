terraform {
  backend "s3" {
    bucket         = "recipe-tfstate"
    key            = "recipe-app.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "recipe-tfstate-lock"
  }

  required_providers {
    aws = {
      version = "~> 3.25.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
